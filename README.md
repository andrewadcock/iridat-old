# Iridat
Iridat is an ecommerce platform for facilitating independent make up brands
business-to-consumer sales. This project utilizes Laravel, and MySQL.


## Code Quality
**PHPCS**: 
* `./vendor/bin/phpcs --standard=phpcs-laravel app/`

**PHPUnit**:
* Log into app server
  * `docker exec -it app-iridat /bin/bash`
* Run PHPUnit Tests
  * php artisan test

## Container / Setup
This project uses Docker for local development. For more details, including 
setup instructions, view `README-CONTAINER.md`
